module Server.WsConnSpec (main, spec) where

import Test.Hspec

import Server.WsConn

main :: IO ()
main = hspec spec

mgr0 :: WsConnManager Int
mgr0 = newWsConnManager

mgr1a, mgr1b, mgr2b :: WsConnManager Int
i1a, i1b, i2b :: Int
(i1a, mgr1a) = addConn 42 mgr0
(i1b, mgr1b) = addConn 37 mgr0
(i2b, mgr2b) = addConn 13 mgr1b

spec :: Spec
spec = do

    describe "new" $
        it "mgr0" $ mgr0 `shouldBe` WsConnManager 0 []

    describe "add" $ do
        it "mgr1a" $ mgr1a `shouldBe` WsConnManager 1 [WsConn 0 42]
        it "i1a" $ i1a `shouldBe` 0
        it "mgr1b" $ mgr1b `shouldBe` WsConnManager 1 [WsConn 0 37]
        it "i1b" $ i1b `shouldBe` 0
        it "mgr2b" $ mgr2b `shouldBe` WsConnManager 2 [WsConn 1 13, WsConn 0 37]
        it "i1a" $ i2b `shouldBe` 1

    describe "rm" $ do
        it "1" $ rmConn 0 mgr0 `shouldBe` WsConnManager 0 []
        it "2" $ rmConn 0 mgr1a `shouldBe` WsConnManager 1 []
        it "3" $ rmConn 42 mgr1a `shouldBe` WsConnManager 1 [WsConn 0 42]
        it "4" $ rmConn 1 mgr2b `shouldBe` WsConnManager 2 [WsConn 0 37]

