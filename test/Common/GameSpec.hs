module Common.GameSpec (main, spec) where

import Test.Hspec

import Common.Game
import qualified Data.Map as Map

main :: IO ()
main = hspec spec

ag0, ag1 :: Agent
ag0 = Agent (Point 1 2) (Color 255 0 0) 0 1
ag1 = Agent (Point 2 2) (Color 255 0 0) 0 1

spec :: Spec
spec = do
    describe "new" $
        it "1" $ newGame `shouldBe` Game Map.empty 0

    describe "step" $ do
        it "1" $ stepAgent 1 ag0 `shouldBe` ag1
        it "2" $ stepGame 1 (Game (Map.fromList [(0,ag0)]) 0)
            `shouldBe` Game (Map.fromList [(0,ag1)]) 1
        it "3" $ stepGame 1 (Game (Map.fromList [(0,ag0), (1,ag0)]) 0)
            `shouldBe` Game (Map.fromList [(0,ag1), (1,ag1)]) 1

