# miso-airconsole

A simple collaborative web app : control and monitor rectangles.

Written in [Haskell](https://www.haskell.org/), using 
[Miso](https://haskell-miso.org/), 
[Servant](https://www.servant.dev/) and 
[Websockets](https://jaspervdj.be/websockets/).

[Try online !](https://miso-airconsole.herokuapp.com)

![](demo.mp4)

## build & run

```
cachix use juliendehos
make client server run-server
```

## build & deploy a docker image

```
make docker-build
make docker-run
```

```
heroku login
heroku container:login
heroku create miso-airconsole
docker tag miso-airconsole:latest registry.heroku.com/miso-airconsole/web
docker push registry.heroku.com/miso-airconsole/web
heroku container:release web --app miso-airconsole
```

