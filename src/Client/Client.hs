{-# LANGUAGE OverloadedStrings #-}

import Common.Game
import Common.Model
import Common.Router
import Common.View
import Common.WsProtocol

import Control.Monad (forM_, when)
import JavaScript.Web.Canvas
import Miso
import Miso.String (ms)
import Network.URI (uriPort, uriRegName, uriScheme)

main :: IO ()
main = miso $ \ currentUri -> do
    let Just host = uriAuthority currentUri
        scheme0 = uriScheme currentUri
        scheme = if null scheme0 then "ws:" else "ws" <> drop 4 scheme0
        uri = URL $ ms $ scheme <> uriRegName host <> uriPort host
        protocols = Protocols []
    App 
      { initialAction = ActionNone
      , model = createModel currentUri
      , update = updateModel
      , view = viewModel
      , events = defaultEvents
      , subs =
            [ uriSub ActionSetUri
            , mouseSub ActionMouseMove
            , websocketSub uri protocols ActionWebSocket
            ]
      , mountPoint = Nothing
      }

uriToProtocol :: Model -> WsProtocol
uriToProtocol m = 
    let modelPath = tail $ uriPath $ modelUri m
        monitorPath = uriPath monitorRoute
        controlPath = uriPath controlRoute
        wsMsg | modelPath == monitorPath = WsMonitorAsk
              | modelPath == controlPath = WsControlAsk
              | otherwise = WsNone
    in wsMsg

-- update

updateModel :: Action -> Model -> Effect Action Model
updateModel ActionNone m = noEff m

updateModel (ActionSetUri uri) m = noEff m { modelUri = uri }
updateModel (ActionChangeUri uri) m = m <# (pushURI uri >> pure ActionNone)

updateModel (ActionWebSocket (WebSocketMessage result)) m = 
    case result of
        WsGame game ->
            let t = floor $ gameTime game
            in m { modelGame = game, modelLog = ms (show (t :: Int)) }
            <# pure ActionRedrawMonitor
        WsColor color ->
            m { modelColor = color }
            <# pure (ActionRedrawControl 0 0)
updateModel (ActionWebSocket (WebSocketError err)) m = 
    noEff m { modelLog = ms (show err) }
updateModel (ActionWebSocket WebSocketClose {}) m = 
    noEff m { modelLog = "close" }
updateModel (ActionWebSocket WebSocketOpen) m = 
    m { modelLog = "open" }
    <# (send (uriToProtocol m) >> pure ActionNone)

updateModel ActionMouseDown m = 
    noEff m { modelControlOn = True }
updateModel ActionMouseUp m = 
    m { modelControlOn = False, modelControlP0 = Nothing }
    <# (send WsActionStop >> pure (ActionRedrawControl 0 0))
updateModel (ActionMouseMove (xx, yy)) m = 
    if modelControlOn m
    then
        let x = fromIntegral xx
            y = fromIntegral yy
        in case modelControlP0 m of
            Nothing -> noEff m { modelControlP0 = Just (Point x y) }
            Just (Point x0 y0) ->
                let dx = x - x0
                    dy = y - y0
                    dir = atan2 dy dx 
                in m <# do
                    when (abs dx + abs dy > 10) $ send (WsActionStart dir)
                    pure $ ActionRedrawControl x y
    else noEff m

updateModel ActionRedrawMonitor m = m <# do
    w <- jsMonitorWidth
    h <- jsMonitorHeight
    ctx <- jsMonitorCtx
    clearRect 0 0 w h ctx
    forM_ (gameAgents $ modelGame m) $ \agent -> do
        let (Color r g b) = agentCol agent
        fillStyle r g b 1.0 ctx
        let (Point x y) = agentPos agent
        fillRect x y 50 50 ctx
    pure ActionNone

updateModel (ActionRedrawControl x1 y1) m = m <# do
    w <- jsControlWidth
    h <- jsControlHeight
    ctx <- jsControlCtx
    let (Color r g b) = modelColor m
    clearRect 0 0 w h ctx
    fillStyle r g b 1.0 ctx
    rect 0 0 w h ctx
    fill ctx
    case modelControlP0 m of
        Nothing -> pure ActionNone
        Just (Point x0 y0) -> do
            rx <- fromIntegral <$> jsControlLeft
            ry <- fromIntegral <$> jsControlTop
            strokeStyle 0 0 0 1.0 ctx
            lineCap LineCapRound ctx
            lineWidth 4 ctx
            beginPath ctx
            moveTo (x0 - rx) (y0 - ry) ctx
            lineTo (x1 - rx) (y1 - ry) ctx
            stroke ctx
            pure ActionNone

-- JS FFI

foreign import javascript unsafe "$r = monitorCanvas.getContext('2d');"
    jsMonitorCtx :: IO Context

foreign import javascript unsafe "$r = monitorCanvas.clientWidth;"
    jsMonitorWidth :: IO Double

foreign import javascript unsafe "$r = monitorCanvas.clientHeight;"
    jsMonitorHeight :: IO Double

foreign import javascript unsafe "$r = controlCanvas.getContext('2d');"
    jsControlCtx :: IO Context

foreign import javascript unsafe "$r = controlCanvas.clientWidth;"
    jsControlWidth :: IO Double

foreign import javascript unsafe "$r = controlCanvas.clientHeight;"
    jsControlHeight :: IO Double

foreign import javascript unsafe "$r = controlCanvas.getBoundingClientRect().left;"
    jsControlLeft :: IO Int

foreign import javascript unsafe "$r = controlCanvas.getBoundingClientRect().top;"
    jsControlTop :: IO Int

