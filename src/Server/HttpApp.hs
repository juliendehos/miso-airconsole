{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Server.HttpApp (httpApp) where

import Common.Model
import Common.Router
import Common.View

import           Data.Proxy (Proxy(..))
import qualified Lucid as L
import           Miso
import           Servant

-- main app

httpApp :: Application
httpApp = serve (Proxy @ServerApi) server

-- server api and app

type ServerApi
    =    ToServerRoutes ClientRoutes HtmlPage Action
    :<|> StaticApi

server :: Server ServerApi
server 
    =    (handleMonitor :<|> handleControl)
    :<|> serveDirectoryWebApp "static"

handleMonitor :: Handler (HtmlPage (View Action))
handleMonitor = pure $ HtmlPage $ monitorView $ createModel monitorRoute

handleControl :: Handler (HtmlPage (View Action))
handleControl = pure $ HtmlPage $ controlView $ createModel controlRoute

-- view rendering

newtype HtmlPage a = HtmlPage a
    deriving (Show, Eq)

instance L.ToHtml a => L.ToHtml (HtmlPage a) where
    toHtmlRaw = L.toHtml
    toHtml (HtmlPage x) = L.doctypehtml_ $ do
        L.head_ $ do
            L.meta_ [L.charset_ "utf-8"]
            L.meta_ [L.name_ "viewport", L.content_ myViewport]
            L.link_ [L.rel_ "stylesheet" , L.href_ myBootstrap]
            L.title_ "airconsole"
            L.with 
                (L.script_ mempty) 
                [L.src_ (mkStatic "all.js"), L.async_ mempty, L.defer_ mempty] 
        L.body_ $ L.toHtml x
        where myViewport = "width=device-width, initial-scale=1, shrink-to-fit=no"
              myBootstrap = "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"

