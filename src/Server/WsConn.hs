module Server.WsConn where

data WsConn a = WsConn
    { wsId :: Int
    , wsConn :: a
    }

instance Eq (WsConn a) where
    c1 == c2 = wsId c1 == wsId c2

data WsConnManager a = WsConnManager
    { wsNextId :: Int
    , wsConns :: [WsConn a]
    } deriving Eq

instance Show (WsConnManager a) where
    show (WsConnManager i cs) = "WsConnManager " ++ show i ++ " " ++ show (map wsId cs)

newWsConnManager :: WsConnManager a
newWsConnManager = WsConnManager 0 []

addConn :: a -> WsConnManager a -> (Int, WsConnManager a)
addConn conn (WsConnManager id0 cs) =
    (id0, WsConnManager (1+id0) (WsConn id0 conn : cs))

rmConn :: Int -> WsConnManager a -> WsConnManager a
rmConn id0 mgr = mgr { wsConns = filter ((/=) id0 . wsId) (wsConns mgr) }

