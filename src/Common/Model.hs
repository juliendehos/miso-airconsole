module Common.Model where

import Common.Game

import Miso.String (ms, MisoString)
import Network.URI (URI)

data Model = Model 
    { modelUri :: URI
    , modelLog :: MisoString
    , modelGame :: Game
    , modelColor :: Color
    , modelControlOn :: Bool
    , modelControlP0 :: Maybe Point
    , modelControlDir :: Double
    } deriving (Eq)

createModel :: URI -> Model
createModel uri = 
    Model uri (ms "no log") newGame newColor False Nothing 0

