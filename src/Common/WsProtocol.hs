{-# LANGUAGE DeriveGeneric #-}

module Common.WsProtocol where

import Common.Game

import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics (Generic)

data WsProtocol
    = WsNone
    | WsMonitorAsk
    | WsControlAsk
    deriving (Eq, Show, Generic)

data WsResult
    = WsGame Game
    | WsColor Color
    deriving (Eq, Show, Generic)

data WsAction
    = WsActionStart Double
    | WsActionStop
    deriving (Eq, Show, Generic)

instance ToJSON WsProtocol
instance ToJSON WsResult
instance ToJSON WsAction

instance FromJSON WsProtocol
instance FromJSON WsResult
instance FromJSON WsAction

