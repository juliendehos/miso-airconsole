{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

module Common.Router where

import Common.WsProtocol

import           Data.Proxy (Proxy(..))
import qualified Data.Text as T
import           Miso
import           Network.URI (URI)
import           Servant.API
import           Servant.Links (safeLink, linkURI)

-- actions

data Action
    = ActionNone
    | ActionSetUri URI
    | ActionChangeUri URI
    | ActionRedrawMonitor
    | ActionRedrawControl Double Double
    | ActionWebSocket (WebSocket WsResult)
    | ActionMouseDown
    | ActionMouseUp
    | ActionMouseMove (Int, Int)
    deriving (Eq)

-- client routes

type MonitorRoute = View Action
type ControlRoute = "control" :> View Action
type ClientRoutes = MonitorRoute :<|> ControlRoute

monitorRoute :: URI
monitorRoute = linkURI $ safeLink clientProxy monitorProxy

controlRoute :: URI
controlRoute = linkURI $ safeLink clientProxy controlProxy

monitorProxy :: Proxy MonitorRoute
monitorProxy = Proxy

controlProxy :: Proxy ControlRoute
controlProxy = Proxy

clientProxy :: Proxy ClientRoutes
clientProxy = Proxy

-- common client/server routes 

type StaticApi = Raw 
type PublicApi = StaticApi 

linkStatic :: URI
linkStatic = linkURI $ safeLink (Proxy @PublicApi) (Proxy @StaticApi)

mkStatic :: T.Text -> T.Text
mkStatic filename = T.concat [T.pack $ show linkStatic, "/", filename]

