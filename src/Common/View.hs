{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Common.View where

import Common.Game
import Common.Model
import Common.Router

import Data.Map (singleton)
import Miso
import Miso.String (ms, MisoString)
import Servant.API

viewControlSize :: MisoString
viewControlSize = "400"

clientViews :: (Model -> View Action) :<|> (Model -> View Action)
clientViews = monitorView :<|> controlView

routeToUrl :: Show a => a -> MisoString
routeToUrl r = ms $ "/" ++ show r 

viewModel :: Model -> View Action
viewModel m = 
    case runRoute clientProxy clientViews modelUri m of
        Left _ -> text "not found"
        Right v -> v

monitorView :: Model -> View Action
monitorView m = div_ [class_ "container"]
    [ navDom
    , monitorDom m
    , aboutDom
    ]

controlView :: Model -> View Action
controlView m = div_ [class_ "container"]
    [ navDom
    , controlDom m
    , aboutDom
    ]

navDom :: View Action
navDom = nav_ [class_ "navbar navbar-expand"]
    [ a_ [class_ "nav-item nav-link", href_ (routeToUrl monitorRoute)]
         [text "Monitor"]
    , a_ [class_ "nav-item nav-link", href_ (routeToUrl controlRoute)]
         [text "Control"]
    ]

aboutDom :: View Action
aboutDom = div_ [class_ "row"]
    [ div_ [class_ "container"] 
        [ hr_ []
        , p_ [] 
            [ text "This web site is implemented in "
            , a_ [href_ "https://haskell.org/"] [text "Haskell"]
            , text ", using "
            , a_ [href_ "https://haskell-miso.org/"] [text "Miso"]
            , text " and "
            , a_ [href_ "https://www.servant.dev/"] [text "Servant"]
            , text ". See the "
            , a_ [href_ "https://gitlab.com/juliendehos/miso-airconsole"]
                 [text "source code"]
            , text "."
            ]
        ]
    ]

monitorDom :: Model -> View Action
monitorDom m = div_ [class_ "row"]
    [ div_ [class_ "container"] 
        [ h1_ [] [text "Airconsole - Monitor"]
        , canvas_ 
            [ id_ "monitorCanvas" , width_ (ms gameWidth), height_(ms gameHeight)
            , style_  (singleton "border" "1px solid black")
            ]
            []
        ]
        , p_ [] [text (modelLog m)]
    ]

controlDom :: Model -> View Action
controlDom m = div_ [class_ "row"]
    [ div_ [class_ "container"] 
        [ h1_ [] [text "Airconsole - Control"]
        , canvas_ 
            [ id_ "controlCanvas"
            , style_  (singleton "border" "1px solid black")
            , width_ viewControlSize, height_ viewControlSize
            , onMouseDown ActionMouseDown, onMouseUp ActionMouseUp
            ]
            []
        ]
        , p_ [] [text (modelLog m)]
    ]

