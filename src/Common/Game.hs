{-# LANGUAGE DeriveGeneric #-}

module Common.Game where

import           Data.Aeson (FromJSON, ToJSON)
import qualified Data.Map as Map
import           GHC.Generics (Generic)
import           System.Random (randomRIO)

gameWidth, gameHeight, gameVelocity :: Double
gameWidth = 800
gameHeight = 600
gameVelocity = 5

data Point = Point Double Double deriving (Eq, Generic, Show)

data Color = Color Int Int Int deriving (Eq, Generic, Show)

data Agent = Agent
    { agentPos :: Point
    , agentCol :: Color
    , agentDir :: Double
    , agentVel :: Double
    } deriving (Eq, Generic, Show)

data Game = Game
    { gameAgents :: Map.Map Int Agent
    , gameTime :: Double
    } deriving (Eq, Generic, Show)

instance ToJSON Point
instance ToJSON Color
instance ToJSON Agent
instance ToJSON Game

instance FromJSON Point
instance FromJSON Color
instance FromJSON Agent
instance FromJSON Game

newColor :: Color
newColor = Color 255 255 255

stepAgent :: Double -> Agent -> Agent
stepAgent dt agent = agent { agentPos = Point px1 py1 }
    where (Point px0 py0) = agentPos agent
          px1 = px0 + cos (agentDir agent) * agentVel agent * dt
          py1 = py0 + sin (agentDir agent) * agentVel agent * dt

stepGame :: Double -> Game -> Game
stepGame dt (Game ags t) = Game (Map.map (stepAgent dt) ags) (t+dt)

newGame :: Game
newGame = Game Map.empty 0

addAgent :: Int -> Agent -> Game -> Game
addAgent iConn agent game0 =
    game0 { gameAgents = Map.insert iConn agent (gameAgents game0) }

rmAgent :: Int -> Game -> Game
rmAgent iConn game0 =
    game0 { gameAgents = Map.delete iConn (gameAgents game0) }

stopAgent :: Int -> Game -> Game
stopAgent iConn game0 =
    let upFunc agent = agent { agentVel = 0 }
    in game0 { gameAgents = Map.adjust upFunc iConn (gameAgents game0) }

startAgent :: Int -> Double -> Game -> Game
startAgent iConn dir game0 =
    let upFunc agent = agent { agentDir = dir, agentVel = gameVelocity }
    in game0 { gameAgents = Map.adjust upFunc iConn (gameAgents game0) }

genAgent :: IO Agent
genAgent = do
    r <- randomRIO (0, 255)
    g <- randomRIO (0, 255)
    b <- randomRIO $ if r+g>400 then (0, 100) else (0, 255)
    x <- (gameWidth/2 +) <$> randomRIO (-200, 200)
    y <- (gameHeight/2 +) <$> randomRIO (-100, 100)
    return $ Agent (Point x y) (Color r g b) 0 0

