{-# LANGUAGE OverloadedStrings #-}

import Common.Game
import Common.WsProtocol

import Control.Monad (forever)
import Data.Aeson (decode, encode)
import Network.Socket (withSocketsDo)
import Network.WebSockets (ClientApp, receiveData, runClient, sendTextData)

main :: IO ()
main = withSocketsDo $ runClient "127.0.0.1" 3000 "/" myApp

myApp :: ClientApp ()
myApp conn = do
    sendTextData conn (encode WsMonitorAsk)
    forever $ do
        m <- decode <$> receiveData conn
        print (m :: Maybe Game)

