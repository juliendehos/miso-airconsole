{-# LANGUAGE OverloadedStrings #-}

import Common.Game
import Common.WsProtocol

import Data.Aeson (decode, encode)
import Network.Socket (withSocketsDo)
import Data.Text (Text)
import Network.WebSockets (ClientApp, runClient, sendClose, sendTextData,
    receiveData)

main :: IO ()
main = withSocketsDo $ runClient "127.0.0.1" 3000 "/" app

app :: ClientApp ()
app conn = do
    putStrLn "Connected!"
    sendTextData conn (encode WsControlAsk)
    m <- decode <$> receiveData conn
    print (m :: Maybe Game)
    _ <- getLine
    sendClose conn ("Bye!" :: Text)

