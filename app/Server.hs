import Server.HttpApp
import Server.WsApp

import Control.Concurrent (newMVar, forkIO)
import Data.Maybe (fromMaybe)
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdout)
import System.Environment (lookupEnv)

main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    putStrLn $ "listening port " ++ show port ++ "..."
    wsModel <- newWsModel
    wsModelVar <- newMVar wsModel
    _ <- forkIO $ loopWsModel wsModelVar
    run port 
        $ logStdout 
        $ wsApp wsModelVar
        $ gzip def { gzipFiles = GzipCompress }
        httpApp

