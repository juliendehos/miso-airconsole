client:
	nix-shell -A client --run "cabal --builddir=dist-client --config-file=config/client.config build client"
	mkdir -p static
	ln -sf ../`find dist-client/ -name all.js` static/

server:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config build server"

client-monitor:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config build client-monitor"

client-control:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config build client-control"

run-server:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config run server"

run-client-monitor:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config run client-monitor"

run-client-control:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config run client-control"

run-test:
	nix-shell -A server --run "cabal --builddir=dist-server --config-file=config/server.config test"

docker-build:
	nix-build nix/docker.nix
	docker load < result

docker-run:
	docker run --rm -it -p 3000:3000 miso-airconsole:latest

clean:
	rm -rf dist-* static/all.js 

